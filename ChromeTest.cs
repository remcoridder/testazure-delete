﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;

namespace NUnitTestProject1
{

    [TestFixture]
     public class Testing
     {
         private OpenQA.Selenium.IWebDriver driver;

         [SetUp]
         public void SetUp()
         {
             driver = new ChromeDriver();
         }

         [Test]
         public void NunitTest()
         {
            string url = "https://www.knab.nl/";
            driver.Navigate().GoToUrl(url);

            Assert.AreEqual(driver.Url, url);
         }

         [TearDown]
         public void TearDown()
         {
            driver.Quit();
         }
     }
}
